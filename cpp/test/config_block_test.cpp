#include <string>
#include <cstdio>
#include <fstream>
#include <config/config_block.h>

int main(int argc, char const *argv[])
{
    std::string config_dir(getenv("CONFIG_DIR"));
    std::string config_file = config_dir + "/channels.lua";

    printf("Reading config from %s\n", config_file.c_str());

    std::ifstream f(config_file.c_str(), std::ios::in | std::ios::binary);
    if (f)
    {
      std::string contents;
      f.seekg(0, std::ios::end);
      contents.resize(f.tellg());
      f.seekg(0, std::ios::beg);
      f.read(&contents[0], contents.size());
      f.close();

      hwo::ConfigBlock config;
      config.load(contents, "Params");

      int global_param;
      if (!config.get("global_param", global_param)) {
        fprintf(stderr, "Failed to retrieve 'global_param'\n");
      }
      printf("global_param: %d\n", global_param);

      for (const auto& node : {"PlannerNode", "TrackerNode", "ParamEstimatorNode"})
      {
        hwo::ConfigBlock planner_conf;
        if (!config.get(node, planner_conf)) {
          fprintf(stderr, "Failed to retrieve 'PlannerNode'\n");
        }

        double tau;
        if (!planner_conf.get("tau", tau)) {
          fprintf(stderr, "Failed to retrieve 'tau'\n");
        }
        else {
          printf("Tau: %0.6f\n", tau);
        }
      }
    }
    return 0;
}