-- Fuck it, channels can be created at run-time on-demand anyway
-- Channels = {
--     { name = "CarTrajectory", type = "CarTrajectory", queue_size = 2 },
--     { name = "CarThrottle", type = "CarThrottle", queue_size = 1 },
--     { name = "TrackParams", type = "TrackParams", queue_size = 5 }
-- }

Params = {

    global_param = 8,

    TrackerNode = -- string keys don't require quotes
    {
        tau = 100.0,
        galaxy = "milky_way",
        answer = 42
    },

    ["PlannerNode"] =   -- but if you must, you need the brackets
    {
        tau = 6.28,
        galaxy = "andromeda",
        answer = 73
    },

    ParamEstimatorNode =
    {
        phi = 1.61,
        galaxy = "milky_way",
        answer = 0
    },

    HwoConnectionNode =
    {
        track_name = "hockenheim",
        car_count = 3
    }
}
