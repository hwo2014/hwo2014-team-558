#ifndef hwo_ParamEstimatorNode_h
#define hwo_ParamEstimatorNode_h

#include <Eigen/Dense>
#include <node.h>
#include <comms/publisher.h>
#include <track/track.h>
#include <msg/car_position_msgs.h>
#include <msg/track_info_msg.h>
#include <msg/vehicle_state_msg.h>

namespace hwo
{

class ParamEstimatorNode : public Node
{
    typedef std::shared_ptr<const car_position_msgs::CarPositionMessage> CarPositionMessagePtr;

public:

    ParamEstimatorNode(const std::string& name,
        const std::shared_ptr<ChannelMap>& channels);

    bool init(ConfigBlock& params);
    bool execute();
    void finish();
    void car_position_callback(const CarPositionMessagePtr& msg);
    void track_info_callback(const std::shared_ptr<const track_info_msgs::TrackInfoMessage>& msg);

private:
    std::vector<car_position_msgs::CarPositionMessage> accum_msgs_;

    std::shared_ptr<Track> track_;
    std::mutex critical_state_;

    std::shared_ptr<Subscriber> car_position_sub_;
    std::shared_ptr<Subscriber> track_info_sub_;

    Publisher vehicle_states_pub_;

    CarPositionMessagePtr prev_car_positions_;
    CarPositionMessagePtr curr_car_positions_;

    void fill_vehicle_states_msg(VehicleStatesMessage& msg);
};

} // namespace hwo

#endif
