#include <chrono>
#include <iostream>
#include <config/config_block.h>
#include <perception/param_estimator_node.h>

using namespace hwo::car_position_msgs;
using namespace hwo::track_info_msgs;
namespace hwo
{

ParamEstimatorNode::ParamEstimatorNode(const std::string& name, 
                                       const std::shared_ptr<ChannelMap>& channels) :
    Node(name, channels)
{
}

bool ParamEstimatorNode::init(ConfigBlock& params)
{
    std::unique_lock<std::mutex> lock(critical_state_);

    printf("[ParamEstimatorNode] Initializing...\n");
    car_position_sub_ = subscribe("carPositions", &ParamEstimatorNode::car_position_callback, this);
    printf("[ParamEstimatorNode] Subscribed to carPositions!\n");

    track_info_sub_ = subscribe("trackInfo", &ParamEstimatorNode::track_info_callback, this);
    printf("[ParamEstimatorNode] Subscribed to trackInfo!\n");

    vehicle_states_pub_ = advertise("VehicleStates");

    return true;
}

void ParamEstimatorNode::car_position_callback(const std::shared_ptr<const CarPositionMessage>& msg)
{
    std::unique_lock<std::mutex> lock(critical_state_);
    prev_car_positions_ = curr_car_positions_;
    curr_car_positions_ = msg;

    if (curr_car_positions_ && track_) { 
        VehicleStatesMessage vehicle_states_msg;
        fill_vehicle_states_msg(vehicle_states_msg);
        // printf("Publishing Vehicle States Message with state for %zd vehicles\n", vehicle_states_msg.vehicle_states.size());
        // printf("    First Car Velocity: %0.6f\n", vehicle_states_msg.vehicle_states.front().speed_mps);
        vehicle_states_pub_.publish(vehicle_states_msg);
    }
}

void ParamEstimatorNode::track_info_callback(const std::shared_ptr<const TrackInfoMessage>& msg)
{
    std::unique_lock<std::mutex> lock(critical_state_);
    printf("[ParamEstimatorNode] Received Track information.\n");
    track_ = msg->track;
    track_->printInfo();
}

bool ParamEstimatorNode::execute()
{
    namespace chrono = std::chrono;

    static chrono::time_point<chrono::high_resolution_clock> start(chrono::high_resolution_clock::now());
    static chrono::time_point<chrono::high_resolution_clock> last_tick = start;
    static chrono::duration<double> d(4.0);

    if (chrono::high_resolution_clock::now() < start + d) {
        chrono::time_point<chrono::high_resolution_clock> now = chrono::high_resolution_clock::now();
        if (now > last_tick + chrono::duration<double>(1.0)) {
            printf("[Param Estimator Node] Tick...\n");
            last_tick = now;
        }
        return true;
    }

    return false;
}

void ParamEstimatorNode::finish()
{
    printf("[Param Estimator Node] Cleaning up...\n");
}

void ParamEstimatorNode::fill_vehicle_states_msg(VehicleStatesMessage& msg)
{
    // Under lock from car_position_callback

    const std::vector<CarPosition>& car_positions = curr_car_positions_->car_positions;

    for (size_t i = 0; i < curr_car_positions_->car_positions.size(); ++i) {
        VehicleStateMessage vehicle_state;

        // Forward car position information
        const CarPosition& car_position = curr_car_positions_->car_positions[i];
        vehicle_state.car_position = car_position;

        // Construct vehicle speed information if we've received at least two messages
        if (prev_car_positions_) {
            const std::vector<CarPosition>& prev_car_positions = prev_car_positions_->car_positions;
            if (prev_car_positions.size() == car_positions.size()) {
                const CarPosition& prev_car_position = prev_car_positions[i]; // TODO: assert that car positions always reported in the same order?

                // simple derivative calculation to get the speed
                const double delta_pos = (track_->track_position(car_position.piece_position) -
                                          track_->track_position(prev_car_position.piece_position));
                const double delta_time = std::chrono::duration<double>(curr_car_positions_->stamp - prev_car_positions_->stamp).count();
                vehicle_state.speed_mps = delta_pos / delta_time;
                int game_tick_diff = curr_car_positions_->game_tick -
                prev_car_positions_->game_tick;
                // printf("(Position: ) %f\n",
                //     track_->track_position(car_position.piece_position));

                // printf("(Speed) : %f\n", delta_pos / game_tick_diff);

                if (vehicle_state.speed_mps < 0) {
                    fprintf(stderr, "WARN: vehicle is going backwards (%0.3f / %0.3f = %0.3f m/s)\n",
                            delta_pos, delta_time, vehicle_state.speed_mps);
                }
                if (vehicle_state.speed_mps == 0) {
                    fprintf(stderr, "WARN: vehicle has halted (%0.3f / %0.3f = %0.3f m/s)\n",
                            delta_pos, delta_time, vehicle_state.speed_mps);
                }

            }
            else {
                fprintf(stderr, "Expected equal number of reported car positions between successive car position messages\n");
            }
        }

        msg.vehicle_states.push_back(vehicle_state);
    }
}

} // namespace hwo
