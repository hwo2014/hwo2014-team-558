#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <thread>
#include <vector>
#include <comms/channel_map.h>
#include <node.h>

class game_logic
{
public:

    game_logic(const std::string& host, const std::string& port, const std::string& name, const std::string& key);

private:

    std::vector<std::unique_ptr<hwo::Node>> nodes_;
    std::vector<std::thread> node_threads_;
    std::shared_ptr<hwo::ChannelMap> channel_map_;

    std::string host_;
    std::string port_;
    std::string name_;
    std::string key_;

    void spawn_channels();
    void spawn_nodes();
};

#endif
