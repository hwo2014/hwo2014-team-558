#ifndef hwo_HwoConnectionNode_h
#define hwo_HwoConnectionNode_h

#include <cstdio>
#include <functional>
#include <iostream> ///< For some reason this is needed to make json.hpp not throw a fit
#include <map>
#include <memory>
#include <string>
#include <vector>
#include <jsoncons/json.hpp>
#include <connection.h>
#include <msg/json_msg.h>
#include <node.h>

#include <track/track.h>
#include <track/lane_info.h>
#include <track/piece_info.h>

#include <msg/game_start_msg.h>
#include <msg/car_position_msgs.h>

namespace hwo
{

class HwoConnectionNode : public Node
{
public:

    HwoConnectionNode(const std::string& name, const std::shared_ptr<ChannelMap>& channels);

    bool init(ConfigBlock& params);
    bool execute();
    void finish();

private:

    typedef std::vector<jsoncons::json> msg_vector;

    std::unique_ptr<hwo_connection> connection_;

    typedef std::function<msg_vector(HwoConnectionNode*, const jsoncons::json&)> action_fun;
    const std::map<std::string, action_fun> action_map;

    // std::shared_ptr<TrackInfo> track_;
    std::shared_ptr<Track> track_;

    car_position_msgs::ID our_id_;

    int game_tick_;
    Publisher car_positions_pub_;
    Publisher track_info_pub_;
    Publisher game_start_pub_;
    Publisher game_end_pub_;
    Publisher car_keys_pub_;

    msg_vector react(const jsoncons::json& msg);

    msg_vector on_join(const jsoncons::json& data);
    msg_vector on_your_car(const jsoncons::json& data);
    msg_vector on_game_start(const jsoncons::json& data);
    msg_vector on_car_positions(const jsoncons::json& data);
    msg_vector on_crash(const jsoncons::json& data);
    msg_vector on_game_end(const jsoncons::json& data);
    msg_vector on_error(const jsoncons::json& data);
    msg_vector on_game_init(const jsoncons::json& data);
    msg_vector on_turbo_available(const jsoncons::json& data);

    void json_msg_callback(const std::shared_ptr<const JsonMessage>& msg)
    {
        printf("Received Json message '%s'\n", msg->msg.c_str());
    }
};

} // namespace hwo

#endif
