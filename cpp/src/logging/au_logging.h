#ifndef au_logging_h
#define au_logging_h

#include <cstdio>
#include <cstdarg>
#include <string>
#include <sstream>

inline void au_printf(const char* fmt, const char* prefix, const char* fname, int line, ...)
{
    std::stringstream ss;
    ss << "[" << prefix << "] " << fmt << " [" << fname << "]:[" << line << "]\n";
    va_list args;
    va_start(args, line);
    vprintf(ss.str().c_str(), args);
    va_end(args);
}

#define LOG_NOTICE(prefix, line, file, fmt, ...) \
au_printf(fmt, prefix, file, line, ##__VA_ARGS__);

#define AU_LOG_NOTICE(fmt, ...) \
LOG_NOTICE("NOTICE:AU", __LINE__, __FILE__, fmt, ##__VA_ARGS__)


#endif
