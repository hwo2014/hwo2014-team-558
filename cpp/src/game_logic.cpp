#include <game_logic.h>
#include <config/config_block.h>
#include <node.h>
#include <control/tracker_node.h>
#include <planning/planner_node.h>
#include <perception/param_estimator_node.h>
#include <hwo_connection_node.h>

game_logic::game_logic(
    const std::string& host,
    const std::string& port,
    const std::string& name,
    const std::string& key)
:
    host_(host),
    port_(port),
    name_(name),
    key_(key)
{
    spawn_channels();
    spawn_nodes();
}


void game_logic::spawn_channels()
{
  // TODO: Create channels from config
  channel_map_.reset(new hwo::ChannelMap);

  // NOTE: queue sizes all set to 1 to indicate the fact that i never implemented them. Huzzah!
  channel_map_->add("CarTrajectory", 1);
  channel_map_->add("CarThrottle", 1);
  channel_map_->add("TrackParams", 1);
  channel_map_->add("carPositions", 1);
  channel_map_->add("trackInfo", 1);
  channel_map_->add("GameStart", 1);
  channel_map_->add("GameEnd", 1);
  channel_map_->add("CarKeys", 1);
  channel_map_->add("VehicleTrajectory", 1);
  channel_map_->add("VehicleStates", 1);
}

void game_logic::spawn_nodes()
{
  const char *config_dirstr = getenv("CONFIG_DIR");
  if (!config_dirstr) {
      fprintf(stderr, "$CONFIG_DIR must be set\n");
      return;
  }

  std::string config_dir(config_dirstr);
  std::string config_file = config_dir + "/channels.lua";

  printf("Reading config from %s\n", config_file.c_str());

  std::ifstream f(config_file.c_str(), std::ios::in | std::ios::binary);
  std::string contents;
  if (f)
  {
    f.seekg(0, std::ios::end);
    contents.resize(f.tellg());
    f.seekg(0, std::ios::beg);
    f.read(&contents[0], contents.size());
    f.close();
  }

  //////////////////////////////////////////////////////////////////////////////
  /// MODIFY "STATIC" CONFIG TO INCLUDE COMMAND-LINE ARGUMENTS FOR HWO CONNECTION NODE
  //////////////////////////////////////////////////////////////////////////////

  std::stringstream modified_luaconf;
  modified_luaconf << contents << "\n";
  modified_luaconf << "Params.HwoConnectionNode.host = \"" << host_ << "\"\n";
  modified_luaconf << "Params.HwoConnectionNode.port = \"" << port_ << "\"\n";
  modified_luaconf << "Params.HwoConnectionNode.name = \"" << name_ << "\"\n";
  modified_luaconf << "Params.HwoConnectionNode.key = \""  <<  key_ << "\"\n";

  printf("Modified config:\n");
  printf("%s\n", modified_luaconf.str().c_str());

  //////////////////////////////////////////////////////////////////////////////
  /// Parse global config
  //////////////////////////////////////////////////////////////////////////////

  hwo::ConfigBlock global_conf;
  if (!global_conf.load(modified_luaconf.str(), "Params")) {
    fprintf(stderr, "Failed to load global config\n");
  }

  nodes_.push_back(std::unique_ptr<hwo::Node>(new hwo::TrackerNode("TrackerNode", channel_map_)));
  nodes_.push_back(std::unique_ptr<hwo::Node>(new hwo::PlannerNode("PlannerNode", channel_map_)));
  nodes_.push_back(std::unique_ptr<hwo::Node>(new hwo::ParamEstimatorNode("ParamEstimatorNode", channel_map_)));
  nodes_.push_back(std::unique_ptr<hwo::Node>(new hwo::HwoConnectionNode("HwoConnectionNode", channel_map_)));

  auto spawn = [](const std::unique_ptr<hwo::Node>& node, hwo::ConfigBlock& conf) -> std::function<void(void)>
  {
    auto spawn_node = [&node, &conf]()
    {
      printf("  Spawning %s\n", node->name().c_str());
      if (!node->init(conf)) {
        fprintf(stderr, "%s failed to initialize\n", node->name().c_str());
        return;
      }

      while (node->execute());

      node->finish();
      };

      return spawn_node;
  };

  printf("Spawning nodes...\n");

  /// HACK HACK HACK DOUBLE HACK TRIPLE HACK initializing this vector with the
  /// number of nodes ensures that iterator validity is maintained and config
  /// blocks won't go out of scope before nodes are allowed to interact with
  /// them. TODO: something better than relying on the size of this vector and
  /// node joins happening in this method
  std::vector<hwo::ConfigBlock> configs(nodes_.size());

  int i = 0;
  for (std::unique_ptr<hwo::Node>& node : nodes_) {
    if (!global_conf.get(node->name(), configs[i])) {
        fprintf(stderr, "Failed to retrieve configuration params for '%s'\n", node->name().c_str());
    }
    node_threads_.push_back(std::thread(spawn(node, configs[i])));
    ++i;
  }

  for (std::thread& node_thread : node_threads_) {
    node_thread.join();
  }
}
