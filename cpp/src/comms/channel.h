#ifndef hwo_Channel_h
#define hwo_Channel_h

#include <functional>
#include <list>
#include <memory>
#include <mutex>
#include <string>
#include <vector>
#include <queue>

namespace hwo
{

class Subscriber;

template <typename T>
void delete_arbitrary(void *p)
{
    delete (T*)p;
}

class Channel
{
public:

    typedef std::shared_ptr<void> NothingToSeeHerePtr;
    typedef std::function<void(const NothingToSeeHerePtr&)> SubscriberCallbackFn;

    Channel(const std::string& name, size_t queue_size);

    template <typename MessageType>
    bool push(const MessageType& msg);

    template <typename MessageType>
    bool push(std::unique_ptr<MessageType>&& msg_ptr);

    template <typename MessageType>
    bool pull(MessageType& msg_out);

    template <typename MessageType>
    bool pull(std::shared_ptr<const MessageType>& msg_ptr_out);

    bool pop();

    void register_subscriber(const std::shared_ptr<Subscriber>& sub);

    size_t size() const { return msg_queue_.size(); }

private:

    std::mutex m_critical;
    std::string name_;
    size_t queue_size_;

    // NothingToSeeHerePtr latest_msg_;
    std::queue<NothingToSeeHerePtr> msg_queue_;
    
    // std::vector<SubscriberCallbackFn> callbacks_;
    std::list<std::weak_ptr<Subscriber>> subscribers_;

    template <typename T>
    NothingToSeeHerePtr create_void_ptr(const T& t);

    template <typename T>
    NothingToSeeHerePtr convert_typed_ptr(std::unique_ptr<T>&& msg_ptr);

    template <typename T>
    std::shared_ptr<const T> convert_void_ptr(const std::shared_ptr<void>& p);

    void notify_subscribers();
};

} // namespace hwo

#endif

#include "channel-inl.h"
