#ifndef hwo_Channel_inl_h
#define hwo_Channel_inl_h

namespace hwo
{

template <typename MessageType>
bool Channel::push(const MessageType& msg)
{
    std::unique_lock<std::mutex> lock(m_critical);

    if (msg_queue_.empty()) {
        msg_queue_.push(create_void_ptr(msg));
    }
    else {
        if (&delete_arbitrary<MessageType> != 
            *std::get_deleter<void (*)(void*)>(msg_queue_.front()))
        {
            return false;
        }
        msg_queue_.pop();
        msg_queue_.push(create_void_ptr(msg));
    }
    notify_subscribers();
    return true;
}

template <typename MessageType>
bool Channel::push(std::unique_ptr<MessageType>&& msg_ptr)
{
    std::unique_lock<std::mutex> lock(m_critical);

    if (msg_queue_.empty()) {
        msg_queue_.push(convert_typed_ptr(std::move(msg_ptr)));
    }
    else {
        if (&delete_arbitrary<MessageType> != 
            *std::get_deleter<void (*)(void*)>(msg_queue_.front()))
        {
            return false;
        }
        msg_queue_.pop();
        msg_queue_.push(convert_typed_ptr(std::move(msg_ptr)));
    }
    notify_subscribers();
    return true;
}

template <typename MessageType>
bool Channel::pull(MessageType& msg_out)
{
    std::unique_lock<std::mutex> lock(m_critical);

    if (msg_queue_.empty()) {
        return false;
    }

    if (*std::get_deleter<void (*)(void*)>(msg_queue_.front()) !=
        &delete_arbitrary<MessageType>)
    {
        return false;
    }

    msg_out = (*((MessageType*)(msg_queue_.front().get())));
    return true;
}

template <typename MessageType>
bool Channel::pull(std::shared_ptr<const MessageType>& msg_ptr_out)
{
    std::unique_lock<std::mutex> lock(m_critical);

    if (msg_queue_.empty()) {
        return false;
    }

    if (*std::get_deleter<void (*)(void*)>(msg_queue_.front()) !=
        &delete_arbitrary<MessageType>)
    {
        return false;
    }

    msg_ptr_out = convert_void_ptr<MessageType>(msg_queue_.front());
    return true;
}

template <typename T>
Channel::NothingToSeeHerePtr Channel::create_void_ptr(const T& t)
{
    return NothingToSeeHerePtr((void*)(new T(t)), delete_arbitrary<T>);
}

template <typename T>
Channel::NothingToSeeHerePtr Channel::convert_typed_ptr(std::unique_ptr<T>&& msg_ptr)
{
    return NothingToSeeHerePtr((void*)msg_ptr.release(), delete_arbitrary<T>);
}

template <typename T>
std::shared_ptr<const T> Channel::convert_void_ptr(const NothingToSeeHerePtr& p)
{
    // return std::shared_ptr<const T>((const T*)p.get());
    return std::static_pointer_cast<const T>(p);
}

} // namespace hwo

#endif
