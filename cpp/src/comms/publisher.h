#ifndef hwo_Publisher_h
#define hwo_Publisher_h

#include <cassert>
#include <memory>
#include <comms/channel.h>

namespace hwo
{

class Publisher
{
public:

    Publisher(const std::shared_ptr<Channel>& channel = std::shared_ptr<Channel>());

    template <typename MessageType>
    void publish(const MessageType& message)
    {
        channel_->push(message);
    }

    template <typename MessageType>
    void publish(const std::shared_ptr<MessageType>& message_ptr)
    {
        assert(false);
        // channel_->push(message);
    }

private:

    std::shared_ptr<Channel> channel_;
};

} // namespace hwo

#endif
