#include <comms/publisher.h>

namespace hwo
{

Publisher::Publisher(const std::shared_ptr<Channel>& channel) :
    channel_(channel)
{
}

} // namespace hwo
