#include <comms/channel.h>
#include <comms/subscriber.h>

namespace hwo
{

Channel::Channel(const std::string& name, size_t queue_size) :
    m_critical(),
    name_(name),
    queue_size_(queue_size),
    msg_queue_()//,
    // callbacks_()
{
}

bool Channel::pop()
{
    std::unique_lock<std::mutex> lock(m_critical);
    if (!msg_queue_.empty()) {
        msg_queue_.pop();
        return true;
    }
    return false;
}

void Channel::register_subscriber(const std::shared_ptr<Subscriber>& sub)
{
    std::unique_lock<std::mutex> lock(m_critical);
    this->subscribers_.push_back(sub);
}

void Channel::notify_subscribers()
{
    std::list<std::weak_ptr<Subscriber>>::iterator sit = subscribers_.begin();
    while (sit != subscribers_.end()) {
        const std::weak_ptr<Subscriber>& subscriber = *sit;
        if (subscriber.expired()) {
            printf("Removing subscriber from Channel '%s'\n", name_.c_str());
            std::list<std::weak_ptr<Subscriber>>::iterator rit = sit;
            ++sit;
            subscribers_.erase(rit);
        }
        else {
            auto sp = subscriber.lock();
            sp->notify(msg_queue_.front());
            ++sit;
        }
    }
}

} // namespace hwo