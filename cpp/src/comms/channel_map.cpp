#include <comms/channel_map.h>
#include <comms/channel.h>
#include <comms/subscriber.h>
#include <comms/publisher.h>

namespace hwo
{

ChannelMap::ChannelMap() :
    channels_()
{
}

bool ChannelMap::add(const std::string& name, size_t queue_size)
{
    if (channels_.find(name) != channels_.end()) {
        return false;
    }
    
    channels_[name] = std::shared_ptr<Channel>(new Channel(name, queue_size));
    return true;
}

Publisher ChannelMap::advertise(const std::string& channel)
{
    std::unique_lock<std::mutex> lock(channels_mutex_);
    if (channels_.find(channel) == channels_.end()) {
        add(channel, 1);
    }
    
    return Publisher(channels_[channel]);
}

} // namespace hwo
