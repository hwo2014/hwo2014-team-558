#include <comms/subscriber.h>

namespace hwo
{

Subscriber::Subscriber(
    const std::shared_ptr<Channel>& channel,
    const SubscriberCallbackFn& fn)
:
    channel_(),
    ready_(false),
    shutdown_(false),
    started_(false),
    last_msg_(),
    callback_mutex_(),
    callback_cond_(),
    callback_(fn)//,
    // callback_thread_([this]() { callback_thread(); })
{
}

Subscriber::~Subscriber()
{
    shutdown_ = true;
    callback_cond_.notify_one();
    std::unique_lock<std::mutex> lock(startup_mutex_);
    started_ = true; // prevent callback thread from spawning
    if (callback_thread_.joinable()) {
        callback_thread_.join();
    }
}

void Subscriber::notify(const std::shared_ptr<void>& msg)
{
    {
        std::unique_lock<std::mutex> lock(startup_mutex_);
        if (!started_) {
            callback_thread_ = std::thread([this]() { callback_thread(); });
            started_ = true;
        }
    }

    last_msg_two_ = msg;
    swap_msg_buffer();
    ready_ = true;
    callback_cond_.notify_one();
}

void Subscriber::callback_thread()
{
    std::mutex startup_mutex;

    while (!shutdown_) {
        std::unique_lock<std::mutex> lock(callback_mutex_);
        callback_cond_.wait(lock, [this]() { return ready_ || shutdown_; });
        if (shutdown_) {
            break;
        }

        swap_msg_mutex_.lock();
        std::shared_ptr<void> msg(last_msg_);
        swap_msg_mutex_.unlock();

        callback_(last_msg_);
        ready_ = false;
    }
}

void Subscriber::swap_msg_buffer()
{
    std::unique_lock<std::mutex> lock(swap_msg_mutex_);
    last_msg_.swap(last_msg_two_);
}

} // namespace hwo
