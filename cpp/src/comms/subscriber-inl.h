#ifndef hwo_Subscriber_inl_h
#define hwo_Subscriber_inl_h

namespace hwo
{

template <typename MessageType>
bool Subscriber::read(const std::string& channel, const MessageType& message)
{
    if (!channel_) {
        return false;
    }
    return false;
}

template <typename MessageType>
bool Subscriber::read(const std::shared_ptr<MessageType>& message_ptr)
{
    if (!channel_) {
        return false;
    }
    return false;
}

} // namespace hwo

#endif
