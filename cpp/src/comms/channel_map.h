#ifndef hwo_ChannelMap_h
#define hwo_ChannelMap_h

#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <comms/channel.h>
#include <comms/subscriber.h>
#include <comms/publisher.h>

namespace hwo
{

class ChannelMap
{
public:

    ChannelMap();

    bool add(const std::string& name, size_t queue_size);

    template <typename T>
    std::shared_ptr<Subscriber>
    subscribe(const std::string& channel,
              const std::function<void(const std::shared_ptr<const T>&)>& callback);

    Publisher advertise(const std::string& channel);

private:

    std::mutex channels_mutex_;
    std::map<std::string, std::shared_ptr<Channel>> channels_;
};

} // namespace hwo

#endif

#include "channel_map-inl.h"
