#ifndef hwo_ChannelMap_inl_h
#define hwo_ChannelMap_inl_h

namespace hwo
{

template <typename T>
std::shared_ptr<Subscriber> ChannelMap::subscribe(
    const std::string& channel,
    const std::function<void(const std::shared_ptr<const T>&)>& callback)
{
    std::unique_lock<std::mutex> lock(channels_mutex_);
    if (channels_.find(channel) == channels_.end()) {
        add(channel, 1);
    }

    std::function<void(const std::shared_ptr<void>&)> subscriber_callback =
    [callback](const std::shared_ptr<void>& p)
    {
        callback(std::static_pointer_cast<const T>(p));
    };

    // channels_[channel]->register_subscriber(subscriber_callback);
    std::shared_ptr<Subscriber> s(new Subscriber(channels_[channel], subscriber_callback));

    channels_[channel]->register_subscriber(s);

    return s;
}

} // namespace hwo

#endif
