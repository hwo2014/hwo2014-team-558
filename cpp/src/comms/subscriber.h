#ifndef hwo_Subscriber_h
#define hwo_Subscriber_h

#include <condition_variable>
#include <functional>
#include <memory>
#include <string>
#include <thread>
#include <comms/channel.h>

namespace hwo
{

class Subscriber
{
public:

    typedef std::function<void(const std::shared_ptr<void>&)> SubscriberCallbackFn;

    Subscriber(const std::shared_ptr<Channel>& channel = std::shared_ptr<Channel>(),
               const SubscriberCallbackFn& fn = SubscriberCallbackFn());

    Subscriber(const Subscriber&) = delete;
    Subscriber& operator=(const Subscriber&) = delete;

    ~Subscriber();

    template <typename MessageType>
    bool read(const std::string& channel, const MessageType& message);

    template <typename MessageType>
    bool read(const std::shared_ptr<MessageType>& message_ptr);

    void notify(const std::shared_ptr<void>& msg);

private:

    std::shared_ptr<Channel> channel_;

    bool ready_;
    bool shutdown_;
    bool started_;

    std::mutex startup_mutex_;

    std::mutex swap_msg_mutex_;
    std::shared_ptr<void> last_msg_;
    std::shared_ptr<void> last_msg_two_;

    std::thread callback_thread_;
    std::mutex callback_mutex_;
    std::condition_variable callback_cond_;
    SubscriberCallbackFn callback_;

    void callback_thread();
    void swap_msg_buffer();
};

} // namespace hwo

#endif
