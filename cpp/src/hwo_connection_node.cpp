#include <chrono>
#include <memory>
#include <hwo_connection_node.h>
#include <protocol.h>
#include <msg/track_info_msg.h>
#include <msg/game_end_msg.h>

using namespace hwo_protocol;
using namespace std;
using namespace jsoncons;

#define PRINT_STREAM(stream) \
{\
    std::stringstream ss;\
    ss << stream;\
    printf("%s\n", ss.str().c_str());\
}

namespace hwo
{

HwoConnectionNode::HwoConnectionNode(const std::string& name, const std::shared_ptr<ChannelMap>& channels) :
    Node(name, channels),
    connection_(),
    action_map({
        { "join",           &HwoConnectionNode::on_join },
        { "yourCar",        &HwoConnectionNode::on_your_car },
        { "gameInit",       &HwoConnectionNode::on_game_init },
        { "gameStart",      &HwoConnectionNode::on_game_start },
        { "carPositions",   &HwoConnectionNode::on_car_positions },
        { "crash",          &HwoConnectionNode::on_crash },
        { "gameEnd",        &HwoConnectionNode::on_game_end },
        { "error",          &HwoConnectionNode::on_error },
        { "turboAvailable", &HwoConnectionNode::on_turbo_available }
    })
{
}

bool HwoConnectionNode::init(ConfigBlock& config)
{
    printf("Hwo Connection Node initializes...\n");

    std::string host, port, name, key;

    if (!config.get("host", host) ||
        !config.get("port", port) ||
        !config.get("name", name) ||
        !config.get("key", key))
    {
        fprintf(stderr, "Failed to retrieve connection parameters\n");
        return false;
    }

    try {
        connection_.reset(new hwo_connection(host, port));
    }
    catch (const std::exception& e) {
        fprintf(stderr, "%s\n", e.what());
        return false;
    }

    car_positions_pub_ = advertise("carPositions");
    track_info_pub_ = advertise("trackInfo");
    game_start_pub_ = advertise("GameStart");
    game_end_pub_ = advertise("GameEnd");
    car_keys_pub_ = advertise("CarKeys");
    
    try {
        connection_->send_requests({ make_join(name, key) });
    }
    catch (const std::exception& e) {
        fprintf(stderr, "Failed to send make_join request\n");
        return false;
    }

    return true;
}

bool HwoConnectionNode::execute()
{
    namespace chrono = std::chrono;
    try {
        auto start = chrono::high_resolution_clock::now();
        boost::system::error_code error;
        jsoncons::json response = connection_->receive_response(error);

        if (error == boost::asio::error::eof) {
            printf("Connection closed\n");
            return false;
        }
        else if (error) {
            return false;
        }

        try {
            connection_->send_requests(react(response));
        }
        catch (...) {
            fprintf(stderr, "Error sending request message\n");
            return false;
        }

        auto end = chrono::high_resolution_clock::now();

        chrono::duration<double> diff(end - start);
    }
    catch (const std::exception& e) {
        fprintf(stderr, "Something in Json failed (%s)\n", e.what());
        return false;
    }

    return true;
}

void HwoConnectionNode::finish()
{
}

HwoConnectionNode::msg_vector HwoConnectionNode::on_turbo_available(const
    jsoncons::json& msg)
{
    printf("Turbo available!\n");
    return { turbo_on("That's why they call me Mr. Fahrenheit!") };
}

HwoConnectionNode::msg_vector HwoConnectionNode::react(const jsoncons::json& msg)
{
    const auto& msg_type = msg["msgType"].as<std::string>();
    const auto& data = msg["data"];
    try {
        game_tick_ = msg["gameTick"].as<int>();
    } catch (const std::exception& e){
        ;
    }
    auto action_it = action_map.find(msg_type);
    if (action_it != action_map.end()) {
        return (action_it->second)(this, data);
    }
    else {
        PRINT_STREAM("Unknown message type: " << msg_type);
        return { make_ping() };
    }
}

HwoConnectionNode::msg_vector HwoConnectionNode::on_join(const jsoncons::json& data)
{
    printf("Joined\n");
    return { make_ping() };
}

HwoConnectionNode::msg_vector HwoConnectionNode::on_your_car(const jsoncons::json& data)
{
    our_id_.name = data["name"].as<std::string>();
    our_id_.color = data["color"].as<std::string>();
    car_keys_pub_.publish(our_id_);
    return { make_ping() };
}

HwoConnectionNode::msg_vector HwoConnectionNode::on_game_init(const jsoncons::json& data)
{
    printf("[HWOConnectionNode] Game init\n");

    // track_.reset(new TrackInfo(data["race"]["track"]));

    // Construct pieceInfo
    jsoncons::json pieces_json = data["race"]["track"]["pieces"];
    std::vector<PieceInfo> pieces;
    for (size_t i = 0; i < pieces_json.size(); ++i) {
        jsoncons::json piece_data = pieces_json[i];
        if(piece_data.has_member("length")) {
            // This is a straight piece
            pieces.push_back(
                PieceInfo(
                    piece_data["length"].as<double>(),
                    piece_data.get("switch", false).as<bool>()
                )
            );
        } else {
            // This is a bend
            pieces.push_back(
                PieceInfo(
                    piece_data["radius"].as<double>(),
                    piece_data["angle"].as<double>(),
                    piece_data.get("switch", false).as<bool>()
                )
            );
        }
    }
    // We have the PieceInfo vector. Let's make the PiecesInfo class.
    PiecesInfo pieces_info(pieces);

    // Construct LaneInfo
    jsoncons::json lanes_json = data["race"]["track"]["lanes"];
    std::vector<LaneInfo> lanes;
    for (size_t i = 0; i < lanes_json.size(); i++) {
        jsoncons::json lane_data = lanes_json[i];
        lanes.push_back(
            LaneInfo(
                lane_data["index"].as<int>(),
                lane_data["distanceFromCenter"].as<double>()
            )
        );
    }
    // We have the LaneInfo vector. Let's make the LanesInfo class.
    LanesInfo lanes_info(lanes);

    jsoncons::json track_json = data["race"]["track"];
    // Make the track.
    track_ = std::make_shared<Track>(
        track_json["id"].as<std::string>(),
        track_json["name"].as<std::string>(),
        lanes_info,
        pieces_info
    );

    // Publish track info
    printf("[HWOConnectionNode] Publishing track info msg\n");
    track_info_msgs::TrackInfoMessage track_msg;
    track_msg.track = track_;
    track_msg.stamp = std::chrono::high_resolution_clock::now();
    track_info_pub_.publish(track_msg);

    return { make_ping() };
}

HwoConnectionNode::msg_vector HwoConnectionNode::on_game_start(const jsoncons::json& data)
{
    GameStartMessage msg;
    game_start_pub_.publish(msg);
    return { make_ping() };
}

HwoConnectionNode::msg_vector HwoConnectionNode::on_car_positions(const jsoncons::json& data)
{
    car_position_msgs::CarPositionMessage msg;
    msg.game_tick = game_tick_;
    for (size_t i=0; i < data.size(); i++){
        json car_data = data[i];
        car_position_msgs::CarPosition car_position;
        json id = car_data["id"];
        car_position.id.name = id["name"].as<string>();
        car_position.id.color = id["color"].as<string>();
        car_position.angle = car_data["angle"].as<double>();

        json piecePosition = car_data["piecePosition"];
        car_position.piece_position.lap = piecePosition["lap"].as<int>();
        car_position.piece_position.piece_index = piecePosition["pieceIndex"].as<int>();
        car_position.piece_position.in_piece_distance = piecePosition["inPieceDistance"].as<double>();

        car_position.piece_position.lane.start_lane_index = piecePosition["lane"]["startLaneIndex"].as<double>();
        car_position.piece_position.lane.end_lane_index = piecePosition["lane"]["endLaneIndex"].as<double>();
        msg.car_positions.push_back(car_position);
    }
    msg.stamp = std::chrono::high_resolution_clock::now();

    car_positions_pub_.publish(msg);
    return { make_throttle(0.65) };
}

HwoConnectionNode::msg_vector HwoConnectionNode::on_crash(const jsoncons::json& data)
{
    printf("Someone crashed\n");
    return { make_ping() };
}

HwoConnectionNode::msg_vector HwoConnectionNode::on_game_end(const jsoncons::json& data)
{
    printf("Race ended\n");

    hwo::GameEndMessage game_end_msg;
    game_end_msg.results.clear();

    hwo::GameEndResult game_end_res;

    const auto& results = data["results"];
    for (int i = 0; i < results.size(); ++i) {
        const json& result = results[i];

        const auto& car = result["car"];
        const auto& carres = result["result"];

        game_end_res.car.name = car["name"].as<std::string>();
        game_end_res.car.color = car["color"].as<std::string>();

        if (carres["laps"].is_empty()) {
            game_end_res.result.laps = -1;
            game_end_res.result.ticks = -1;
            game_end_res.result.millis = -1;
        }
        else {
            game_end_res.result.laps = carres["laps"].as<int>();
            game_end_res.result.ticks = carres["ticks"].as<int>();
            game_end_res.result.millis = carres["millis"].as<int>();
        }

        game_end_msg.results.push_back(game_end_res);
    }

    game_end_pub_.publish(game_end_msg);

    return { make_ping() };
}

HwoConnectionNode::msg_vector HwoConnectionNode::on_error(const jsoncons::json& data)
{
    PRINT_STREAM("Error: " << data.to_string());
    return { make_ping() };
}

} // namespace hwo
