#ifndef hwo_MotionPrimitive_h
#define hwo_MotionPrimitive_h

#include <planning/planner_types.h>
#include <track/track.h>

namespace hwo
{

class MotionPrimitive
{
public:

    MotionPrimitive(const DiscreteState& delta_state);

    const DiscreteState& delta_state() const { return delta_state_; }

    std::vector<ContinuousState> create_motion(const ContinuousState& start,
                                               const Track& track);

private:

    DiscreteState delta_state_;
};

} // namespace hwo

#endif