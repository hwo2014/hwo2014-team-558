#ifndef hwo_LanePlanner_h
#define hwo_LanePlanner_h

#include <memory>
#include <vector>
#include <planning/lane_planning_environment.h>
#include <track/track.h>

namespace hwo
{

struct VehicleState
{
    double speed_mps;
    double slip_angle;
    double track_time;
    int lane;
};

struct VehicleProperties
{
    double max_speed;
    double max_accel;
};

struct TrackProperties
{
    Track track;
    double friction;
};

class LanePlanner
{
public:

    enum Result
    {
        SUCCESS,
        FAILED_TO_SET_START,
        FAILED_TO_SET_GOAL,
        FAILED_TO_INIT_SBPL,
        FAILED_TO_PLAN_PATH
    };

    LanePlanner();

    Result make_plan(const VehicleState& start,
                     const VehicleProperties& vehicle_props,
                     const TrackProperties& track_props,
                     double lookahead_m,
                     std::vector<VehicleState>& trajectory_out);

private:

    std::unique_ptr<ARAPlanner> search_;
    std::unique_ptr<LanePlanningEnvironment> env_;
    std::vector<MotionPrimitive> mprims_;

};

} // namespace hwo

#endif
