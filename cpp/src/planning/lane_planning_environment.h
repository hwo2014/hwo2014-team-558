#ifndef hwo_LanePlanningEnvironment_h
#define hwo_LanePlanningEnvironment_h

#include <cassert>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <sbpl/headers.h>
#include <planning/motion_primitive.h>
#include <planning/planner_types.h>

namespace hwo
{

class LanePlanningEnvironment : public DiscreteSpaceInformation
{
public:

    /// @brief Create a LanePlanningEnvironment given the dimensions of the
    ///        state space and templates representing the action space
    LanePlanningEnvironment(int num_vels,
                            int num_times,
                            int num_pos,
                            int num_lanes,
                            double max_vel,
                            double max_time,
                            double max_pos,
                            double max_accel,
                            std::vector<MotionPrimitive>& mprims);

    int GetGoalHeuristic(int stateID);
    void GetSuccs(int SourceStateID,
                  std::vector<int>* SuccIDV,
                  std::vector<int>* CostV);
    void PrintState(int stateID, bool bVerbose, FILE* fOut = NULL);

    int SetStart(const ContinuousState& state);
    int SetGoal(double x);

private:

    ///@{ Auxiliary Graph Structures
    class Action
    {
    public:

        int mprim_index() const { return mprim_index_; }
        int src_state() const { return src_state_; }
        int dst_state() const { return dst_state_; }
        int cost() const { return cost_; }

    private:        

        int mprim_index_;
        int src_state_;
        int dst_state_;
        int cost_;          ///< Invalid if not strictly positive
    };
    /// @}

    int max_states_;

    int start_state_id_;

    int goal_state_id_;
    int goal_x_;

    /// @{ State Space Parameters
    double max_v_;
    double max_t_;
    double max_x_;

    int num_pos_;
    int num_vels_;
    int num_times_;
    int num_lanes_;

    double vel_res_;
    double time_res_;
    double pos_res_;
    /// @}

    /// @{ Cost Function Parameters
    std::vector<MotionPrimitive> mprims_;
    double max_accel_;
    /// @}

    inline bool IsValid(const DiscreteState& state) const;
    inline bool IsValid(const ContinuousState& state) const;
    inline bool IsValid(int state_id) const;

    inline bool WithinBounds(const DiscreteState& state) const;
    inline bool WithinBounds(int state_id) const;

    bool IsGoalState(const DiscreteState& state) const;
    bool IsGoalState(int state_id) const;

    DiscreteState Discretize(const ContinuousState& state) const;
    ContinuousState Continuize(const DiscreteState& state) const;

    int DiscretizeV(double v) const;
    int DiscretizeT(double t) const;
    int DiscretizeX(double x) const;

    double ContinuizeV(int v) const;
    double ContinuizeT(int t) const;
    double ContinuizeX(int x) const;

    std::vector<Action> GetActionsFrom(const DiscreteState& state) const;

    inline int state_to_index(int x, int v, int t, int l) const;
    inline int state_to_index(const DiscreteState& state) const;
    inline DiscreteState index_to_state(int index) const;

    /// @{ Ignored but required SBPL interface
    bool InitializeEnv(const char*);
    bool InitializeMDPCfg(MDPConfig*);
    int GetFromToHeuristic(int, int);
    int GetStartHeuristic(int);
    void GetPreds(int, std::vector<int>*, std::vector<int>*);
    void SetAllActionsandAllOutcomes(CMDPSTATE*);
    void SetAllPreds(CMDPSTATE*);
    int SizeofCreatedEnv();
    void PrintEnv_Config(FILE*);
    void EnsureHeuristicsUpdated(bool bGoalHeuristics);
    /// @}
};

} // namespace hwo

#endif
