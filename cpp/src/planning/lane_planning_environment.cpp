#include <cmath>
#include <planning/lane_planning_environment.h>

namespace hwo
{
    
LanePlanningEnvironment::LanePlanningEnvironment(
    int num_vels,
    int num_times,
    int num_pos,
    int num_lanes,
    double max_vel,
    double max_time,
    double max_pos,
    double max_accel,
    std::vector<MotionPrimitive>& mprims)
:
    max_states_(num_vels * num_times * num_pos * num_lanes),
    start_state_id_(-1),
    goal_state_id_(max_states_),
    goal_x_(0),
    max_v_(max_vel),
    max_t_(max_time),
    max_x_(max_pos),
    num_pos_(num_pos),
    num_vels_(num_vels),
    num_times_(num_times),
    num_lanes_(num_lanes),
    max_accel_(max_accel),
    vel_res_(max_vel / num_vels),
    time_res_(max_time / num_times),
    pos_res_(max_pos / num_pos)
{
    // allocate state id -> planner index mapping upfront
    StateID2IndexMapping.resize(max_states_);
    for (int i = 0; i < max_states_; ++i) {
        int* entry = new int[NUMOFINDICES_STATEID2IND];
        StateID2IndexMapping[i] = entry;
        for (int j = 0; j < NUMOFINDICES_STATEID2IND; ++j) {
            StateID2IndexMapping[i][j] = -1;
        }
    }
}

int LanePlanningEnvironment::GetGoalHeuristic(int state_id)
{
    DiscreteState state = index_to_state(state_id);
    return abs(state.x - goal_x_);
}

void LanePlanningEnvironment::GetSuccs(
    int SourceStateID,
    std::vector<int>* SuccIDV,
    std::vector<int>* CostV)
{
    DiscreteState src_state = index_to_state(SourceStateID);
    for (const auto& action : GetActionsFrom(src_state)) {
        if (action.cost() > 0) {
            if (IsGoalState(action.dst_state())) {
                SuccIDV->push_back(goal_state_id_);
            }
            else {
                SuccIDV->push_back(action.dst_state());
            }
            CostV->push_back(action.cost());
        }
    }
}

void LanePlanningEnvironment::PrintState(int stateID, bool bVerbose, FILE* fOut)
{
    if (fOut) {
        fprintf(fOut, "State %d: %s\n", stateID, to_string(index_to_state(stateID)).c_str());
    }
    else {
        printf("State %d: %s\n", to_string(index_to_state(stateID)).c_str());
    }
}

int LanePlanningEnvironment::SetStart(const ContinuousState& state)
{
    DiscreteState disc_start = Discretize(state);

    if (!WithinBounds(disc_start)) {
        fprintf(stderr, "State %s is out of bounds\n", to_string(state).c_str());
        return -1;
    }

    if (!IsValid(state)) {
        fprintf(stderr, "Continuous State %s is out of bounds\n", to_string(state).c_str());
        return -1;
    }

    if (!IsValid(disc_start)) {
        fprintf(stderr, "Discretize State %s is out of bounds\n", to_string(state).c_str());
        return -1;
    }

    return (start_state_id_ = state_to_index(disc_start));
}

int LanePlanningEnvironment::SetGoal(double x)
{
    DiscreteState disc_goal(DiscretizeX(x), 0, 0, 0); // NOTE: 0 is always a valid coordinate

    if (!WithinBounds(disc_goal)) {
        fprintf(stderr, "Goal Position %d is out of bounds\n");
        return -1;
    }

    goal_x_ = DiscretizeX(x);
    return max_states_;
}

int LanePlanningEnvironment::DiscretizeV(double v) const
{
    return (int)round(v / vel_res_);
}

int LanePlanningEnvironment::DiscretizeT(double t) const
{
    return (int)round(t / time_res_);
}

int LanePlanningEnvironment::DiscretizeX(double x) const
{
    return (int)round(x / pos_res_);
}

double LanePlanningEnvironment::ContinuizeV(int v) const
{
    return v * vel_res_;
}

double LanePlanningEnvironment::ContinuizeT(int t) const
{
    return t * time_res_;
}

double LanePlanningEnvironment::ContinuizeX(int x) const
{
    return x * pos_res_;
}

bool LanePlanningEnvironment::IsValid(const DiscreteState& state) const
{
    return false;
}

bool LanePlanningEnvironment::IsValid(const ContinuousState& state) const
{
    return false;
}

bool LanePlanningEnvironment::IsValid(int state_id) const
{
    return IsValid(index_to_state(state_id));
}

bool LanePlanningEnvironment::WithinBounds(const DiscreteState& state) const
{
    return state.x >= 0 && state.x < num_pos_ &&
           state.v >= 0 && state.v < num_vels_ &&
           state.t >= 0 && state.t < num_times_ &&
           state.l >= 0 && state.l < num_lanes_;
    return false;
}

bool LanePlanningEnvironment::WithinBounds(int state_id) const
{
    return WithinBounds(index_to_state(state_id));
}

bool LanePlanningEnvironment::IsGoalState(const DiscreteState& state) const
{
    return state.x == goal_x_;
}

bool LanePlanningEnvironment::IsGoalState(int state_id) const
{
    return IsGoalState(index_to_state(state_id));
}

DiscreteState LanePlanningEnvironment::Discretize(const ContinuousState& state) const
{
    return DiscreteState(DiscretizeX(state.x),
                         DiscretizeV(state.v),
                         DiscretizeT(state.t),
                         state.l);
}

ContinuousState LanePlanningEnvironment::Continuize(const DiscreteState& state) const
{
    return ContinuousState(ContinuizeX(state.x),
                           ContinuizeV(state.v),
                           ContinuizeT(state.t),
                           state.l);
}

std::vector<LanePlanningEnvironment::Action>
LanePlanningEnvironment::GetActionsFrom(const DiscreteState& state) const
{
    return { };
}

int LanePlanningEnvironment::state_to_index(int x, int v, int t, int l) const
{
    return l * num_pos_ * num_vels_ * num_times_ + t * num_pos_ * num_vels_ + v * num_pos_ + x;
}

int LanePlanningEnvironment::state_to_index(const DiscreteState& state) const
{
    return state_to_index(state.x, state.v, state.t, state.l);
}

DiscreteState LanePlanningEnvironment::index_to_state(int index) const
{
    int l = (index - (0)) / (num_pos_ * num_vels_ * num_times_);
    int t = (index - (l * num_pos_ * num_vels_ * num_times_)) / (num_pos_ * num_vels_);
    int v = (index - (l * num_pos_ * num_vels_ * num_times_) - (t * num_pos_ * num_vels_)) / (num_pos_);
    int x = (index - (l * num_pos_ * num_vels_ * num_times_) - (t * num_pos_ * num_vels_) - (v * num_pos_));
    return DiscreteState(x, v, t, l);
}

////////////////////////////////////////////////////////////////////////////////
// Ignored SBPL Interface
////////////////////////////////////////////////////////////////////////////////

bool LanePlanningEnvironment::InitializeEnv(const char*)
{
    printf("Zapdos used Zap Cannon!!!\n");
    return false;
}

bool LanePlanningEnvironment::InitializeMDPCfg(MDPConfig *)
{
    printf("Charizard used Flamethrower!!!\n");
    return false;
}

int LanePlanningEnvironment::GetFromToHeuristic(int, int)
{
    printf("Nidoran used Poison Sting!!!\n");
    return 0;
}

int LanePlanningEnvironment::GetStartHeuristic(int)
{
    printf("Gyarados used Dragon Rage!!!\n");
    return 0;
}

void LanePlanningEnvironment::GetPreds(int, std::vector<int>*, std::vector<int>*)
{
    printf("Snorlax used Rest!!!\n");
}
 
void LanePlanningEnvironment::SetAllActionsandAllOutcomes(CMDPSTATE*)
{
    printf("Butterfree used Confusion!!!\n");
}

void LanePlanningEnvironment::SetAllPreds(CMDPSTATE*)
{
    printf("Clefairy used Metronome!!!\n");
}

int LanePlanningEnvironment::SizeofCreatedEnv()
{
    printf("Pikachu used Agility!!!\n");
    return -1;
}

void LanePlanningEnvironment::PrintEnv_Config(FILE*)
{
    printf("Haunter used Lick!!!\n");
}

void LanePlanningEnvironment::EnsureHeuristicsUpdated(bool bGoalHeuristics)
{
    printf("Starmie used Psychic!!!\n");
}

} // namespace hwo
