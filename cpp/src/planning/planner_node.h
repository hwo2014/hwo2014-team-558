#ifndef hwo_PlannerNode_h
#define hwo_PlannerNode_h

#include <cstdio>
#include <mutex>
#include <msg/dummy_msg.h>
#include <planning/lane_planner.h>
#include <node.h>
#include <msg/car_position_msgs.h>
#include <track/track_info.h>
#include <msg/track_info_msg.h>
#include <logging/au_logging.h>
#include <msg/game_start_msg.h>
#include <msg/game_end_msg.h>

namespace hwo
{

class PlannerNode : public Node
{
    typedef std::shared_ptr<const car_position_msgs::CarPositionMessage> CarPositionMessagePtr;
    typedef std::shared_ptr<const track_info_msgs::TrackInfoMessage> TrackInfoMessagePtr;
    typedef std::shared_ptr<const GameStartMessage> GameStartMessagePtr;
    typedef std::shared_ptr<const GameEndMessage> GameEndMessagePtr;
    typedef std::shared_ptr<const car_position_msgs::ID> CarKeysMessagePtr;

public:

    PlannerNode(const std::string& name, const std::shared_ptr<ChannelMap>& channels);

    bool init(ConfigBlock& config);
    bool execute();
    void finish();

private:

    bool race_started_;
    bool race_ended_;
    bool ready_;
    LanePlanner planner_;
    std::shared_ptr<Subscriber> track_info_sub_;
    std::shared_ptr<Subscriber> car_positions_sub_;
    std::shared_ptr<Subscriber> game_start_sub_;
    std::shared_ptr<Subscriber> game_end_sub_;
    std::shared_ptr<Subscriber> car_keys_sub_;

    std::mutex critical_state_;

    CarPositionMessagePtr latest_carpos_msg_;
    CarKeysMessagePtr car_keys_;
    TrackInfoMessagePtr latest_track_info_;

    Publisher vehicle_traj_pub_;

    void car_positions_callback(const CarPositionMessagePtr& msg);
    void track_info_callback(const TrackInfoMessagePtr& msg);
    void game_start_callback(const GameStartMessagePtr& msg);
    void game_end_callback(const GameEndMessagePtr& msg);
    void car_keys_callback(const CarKeysMessagePtr& msg);

    bool ready_to_plan();
};

} // namespace hwo

#endif
