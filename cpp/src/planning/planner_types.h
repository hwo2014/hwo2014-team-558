#ifndef hwo_planner_types_h
#define hwo_planner_types_h

#include <string>

namespace hwo
{

struct DiscreteState
{
    DiscreteState();
    DiscreteState(int x, int v, int t, int l);

    int x; ///< position along track
    int v; ///< velocity
    int t; ///< time
    int l; ///< lane index
};

struct ContinuousState
{
    ContinuousState();
    ContinuousState(double x, double v, double t, int l);

    double v;
    double t;
    double x;
    int l;
};

std::string to_string(const DiscreteState& state);
std::string to_string(const ContinuousState& state);

} // namespace hwo

#endif
