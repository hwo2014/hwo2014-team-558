#include <cstdio>
#include <chrono>
#include <functional>
#include <planning/planner_node.h>
#include <msg/vehicle_trajectory_msg.h>

namespace hwo
{

PlannerNode::PlannerNode(const std::string& name,
                         const std::shared_ptr<ChannelMap>& channels) :
    Node(name, channels),
    race_started_(false),
    race_ended_(false),
    ready_(false),
    track_info_sub_(),
    car_positions_sub_(),
    game_start_sub_(),
    game_end_sub_(),
    critical_state_()
{
}

bool PlannerNode::init(ConfigBlock& params)
{
    AU_LOG_NOTICE("Initializing...");

    double tau;
    if (!params.get("tau", tau)) {
        fprintf(stderr, "Failed to retrieve 'tau'\n");
    }

    track_info_sub_ = subscribe("trackInfo", &PlannerNode::track_info_callback, this);
    car_positions_sub_ = subscribe("carPositions", &PlannerNode::car_positions_callback, this);
    game_start_sub_ = subscribe("GameStart", &PlannerNode::game_start_callback, this);
    game_end_sub_ = subscribe("GameEnd", &PlannerNode::game_end_callback, this);
    car_keys_sub_ = subscribe("CarKeys", &PlannerNode::car_keys_callback, this);

    vehicle_traj_pub_ = advertise("VehicleTrajectory");

    return true;
}

bool PlannerNode::execute()
{
    if (race_ended_) {
        return false;
    }

    if (!ready_) {
        if (ready_to_plan()) {
            ready_ = true; 
            AU_LOG_NOTICE("Done waiting to receive our car keys and initial positions");
        }
        else {
            return true; // busy bee
        }
    }

    namespace chrono = std::chrono;
    auto start = chrono::high_resolution_clock::now();

    // grab all the latest messages so that they don't get swapped out from under us
    CarPositionMessagePtr car_positions;
    CarKeysMessagePtr car_keys;
    TrackInfoMessagePtr track_info;
    {
        std::unique_lock<std::mutex> lock(critical_state_);
        car_positions = latest_carpos_msg_;
        car_keys = car_keys_; // Shouldn't expect this to get swapped out, but you just never freaking know
        track_info = latest_track_info_;
    }

    // find our car
    size_t our_car_index = 0;
    for (; our_car_index < car_positions->car_positions.size(); ++our_car_index) {
        const car_position_msgs::CarPosition& car_position = car_positions->car_positions[our_car_index];
        if (car_position.id.name == car_keys->name &&
            car_position.id.color == car_keys->color)
        {
            // AU_LOG_NOTICE("Found our car!");
            break;
        }
    }

    if (our_car_index == car_positions->car_positions.size()) {
        AU_LOG_NOTICE("WTF! WHERE IS OUR FUCKING CAR!?");
        return true;
    }

    const car_position_msgs::CarPosition& our_car = car_positions->car_positions[our_car_index];

    VehicleState curr_state;
    curr_state.speed_mps = 1.0;
    curr_state.slip_angle = our_car.angle;
    curr_state.track_time = track_info->track->track_position(our_car.piece_position);
    // assume that we're starting in the other lane so we don't plan from the starting lane when in the middle of a lane switch
    curr_state.lane = our_car.piece_position.lane.end_lane_index; 

    std::vector<VehicleState> traj_out;
    LanePlanner::Result res = planner_.make_plan(curr_state, VehicleProperties(), TrackProperties(), 0.0, traj_out);
    if (res != LanePlanner::SUCCESS) {
        AU_LOG_NOTICE("COME THE FUCK ON PLANNER");
    }

    VehicleTrajectoryMessage traj_msg;
    vehicle_traj_pub_.publish(traj_msg);

    auto end = chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_diff = (end - start);
    // AU_LOG_NOTICE("Planner runs at %0.6f Hz", 1.0 / time_diff.count());
    return true;
}

void PlannerNode::car_positions_callback(const CarPositionMessagePtr& msg)
{
    std::unique_lock<std::mutex> lock(critical_state_);
    latest_carpos_msg_ = msg;
}

void PlannerNode::track_info_callback(const TrackInfoMessagePtr& msg)
{
    std::unique_lock<std::mutex> lock(critical_state_);
    latest_track_info_ = msg;
}

void PlannerNode::game_start_callback(const GameStartMessagePtr& msg)
{
    std::unique_lock<std::mutex> lock(critical_state_);
    AU_LOG_NOTICE("Game has started!");
    race_started_ = true;
}

void PlannerNode::game_end_callback(const GameEndMessagePtr& msg)
{
    std::unique_lock<std::mutex> lock(critical_state_);
    AU_LOG_NOTICE("Game has ended!");
    race_ended_ = true;
}

void PlannerNode::car_keys_callback(const CarKeysMessagePtr& msg)
{
    std::unique_lock<std::mutex> lock(critical_state_);
    AU_LOG_NOTICE("Got the keys to my brand new %s %s", msg->color.c_str(), msg->name.c_str());
    car_keys_ = msg;
}

void PlannerNode::finish()
{
    AU_LOG_NOTICE("Cleaning up...");
}

bool PlannerNode::ready_to_plan()
{
    std::unique_lock<std::mutex> lock(critical_state_);
    return latest_carpos_msg_ && car_keys_ && race_started_;
}

} // namespace hwo
