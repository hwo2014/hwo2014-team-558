#include <planning/motion_primitive.h>

namespace hwo
{

MotionPrimitive::MotionPrimitive(const DiscreteState& delta_state) :
    delta_state_(delta_state)
{
}

std::vector<ContinuousState> MotionPrimitive::create_motion(
    const ContinuousState& start,
    const Track& track)
{
    return { };
}

} // namespace hwo
