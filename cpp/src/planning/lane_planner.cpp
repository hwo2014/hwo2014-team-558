#include <planning/lane_planner.h>
#include <planning/planner_types.h>
#include <logging/au_logging.h>

namespace hwo
{

LanePlanner::LanePlanner() :
    env_(),
    mprims_()
{

}

auto LanePlanner::make_plan(
    const VehicleState& start,
    const VehicleProperties& vehicle_props,
    const TrackProperties& track_props,
    double lookahead_m,
    std::vector<VehicleState>& trajectory_out) -> Result
{
    // AU_LOG_NOTICE("Car is currently at %0.3f m going %0.3f m/s", start.track_time, start.speed_mps);
    // TODO: generate motion primitives

    // env_->SetStart(ContinuousState(start.track_time, start.speed_mps, 0, start.lane));
    // env_->SetGoal(start.track_time + 30);
    // // search_->replan();

    // trajectory_out.clear(); // lol
    
    return SUCCESS;
};

} // namespace hwo
