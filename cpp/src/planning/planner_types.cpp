#include <sstream>
#include <planning/planner_types.h>

namespace hwo
{

DiscreteState::DiscreteState() : x(), v(), t(), l() { }

DiscreteState::DiscreteState(int x, int v, int t, int l) :
    x(x),
    v(v),
    t(t),
    l(l)
{
}

ContinuousState::ContinuousState() : x(), v(), t(), l() { }

ContinuousState::ContinuousState(double x, double v, double t, int l) :
    x(x),
    v(v),
    t(t),
    l(l)
{
}

std::string to_string(const DiscreteState& state)
{
    std::stringstream ss;
    ss << "("
       << "x: " << state.x << ", "
       << "v: " << state.v << ", "
       << "t: " << state.v << ", "
       << "l: " << state.l << ")";
    return ss.str();
}

std::string to_string(const ContinuousState& state)
{
    std::stringstream ss;
    ss << "("
       << "x: " << state.x << ", "
       << "v: " << state.v << ", "
       << "t: " << state.v << ", "
       << "l: " << state.l << ")";
    return ss.str();
}

} // namespace hwo
