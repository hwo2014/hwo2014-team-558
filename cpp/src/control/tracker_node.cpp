#include <cstdio>
#include <chrono>
#include <control/tracker_node.h>
#include <msg/dummy_msg.h>

namespace hwo
{

TrackerNode::TrackerNode(const std::string& name, const std::shared_ptr<ChannelMap>& channels) :
    Node(name, channels)
{
}

bool TrackerNode::init(ConfigBlock& params)
{
    printf("[Tracker Node] Initializing...\n");

    return true;
}

bool TrackerNode::execute()
{
    static Publisher pub = advertise("Dummy");

    namespace chrono = std::chrono;

    static chrono::time_point<chrono::high_resolution_clock> start(chrono::high_resolution_clock::now());
    static chrono::time_point<chrono::high_resolution_clock> last_tick = start;
    static chrono::duration<double> d(4.0);

    if (chrono::high_resolution_clock::now() < start + d) {
        chrono::time_point<chrono::high_resolution_clock> now = chrono::high_resolution_clock::now();
        if (now > last_tick + chrono::duration<double>(1.0)) {
            printf("[Tracker Node] Tick...\n");
            pub.publish(DummyMessage());
            last_tick = now;
        }
        return true;
    }

    return false;
}

void TrackerNode::finish()
{
    printf("[Tracker Node] Cleaning up...\n");
}

} // namespace hwo

