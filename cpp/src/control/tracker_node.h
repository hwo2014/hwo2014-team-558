#ifndef hwo_TrackerNode_h
#define hwo_TrackerNode_h

#include <node.h>

namespace hwo
{

class TrackerNode : public Node
{
public:

    TrackerNode(const std::string& name, const std::shared_ptr<ChannelMap>& channels);

    bool init(ConfigBlock& params);
    bool execute();
    void finish();
};

} // namespace hwo

#endif
