#ifndef hwo_Node_h
#define hwo_Node_h

#include <functional>
#include <memory>
#include <string>
#include <vector>
#include <comms/channel_map.h>
#include <config/config_block.h>
#include <comms/subscriber.h>

namespace hwo
{

class Node
{
public:

    Node(const std::string& name, const std::shared_ptr<ChannelMap>& channels);

    virtual bool init(ConfigBlock& params) = 0;
    virtual bool execute() = 0;
    virtual void finish() = 0;

    const std::string& name() const { return name_; };

    void spin();

    template <typename T>
    std::shared_ptr<Subscriber> subscribe(const std::string& channel,
                         const std::function<void(const std::shared_ptr<const T>&)>& callback)
    {
        return channels_->subscribe(channel, callback);
    }

    template <typename T, typename C>
    std::shared_ptr<Subscriber> subscribe(
            const std::string& channel,
            void (C::*callback)(const std::shared_ptr<const T>&), C* c)
    {
       std::function<void(const std::shared_ptr<const T>&)> fun =
                [c, callback](const std::shared_ptr<const T>& msg) { (c->*callback)(msg); };

        return channels_->subscribe(channel, fun);
    }

    Publisher advertise(const std::string& channel)
    {
        return channels_->advertise(channel);
    }

private:

    std::string name_;
    std::shared_ptr<ChannelMap> channels_;
};

} // namespace hwo

#endif
