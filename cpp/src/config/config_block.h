#ifndef hwo_ConfigBlock_h
#define hwo_ConfigBlock_h

#include <memory>
#include <string>
#include <vector>
#include <istream>
#include <lua.hpp>

namespace hwo
{

class ConfigBlock
{
public:

    ConfigBlock();
    ~ConfigBlock();

    // disallow copy and assign
    ConfigBlock(const ConfigBlock&) = delete;
    ConfigBlock& operator=(const ConfigBlock&) = delete;

    /// @brief Load the ConfigBlock from the global value @confparam in the Lua program represented by @luaconf
    bool load(const std::string& luaconf, const std::string& confparam);

    bool get(const std::string& param, int& val);

    bool get(const std::string& param, double& val);

    bool get(const std::string& param, bool& val);

    bool get(const std::string& param, std::string& val);

    bool get(const std::string& param, ConfigBlock& config);

private:

    typedef std::unique_ptr<lua_State, void (*)(lua_State*)> LuaStatePtr;

    // maintain the source that was used to initialize lua so that we can clone the lua state
    std::string luaconf_;               

    // for some reason i decided to keep the stack of all the table names insteaad of just cloning the lua stack directly which is probably kinda maybe sorta doable
    std::vector<std::string> tables_;   

    // each config block maintains its own lua state
    LuaStatePtr L_;

    // clone a new lua state
    LuaStatePtr clone_state();

    bool trace_tables(lua_State *L, const std::vector<std::string>& tables);
};

} // namespace hwo

#endif
