#include <iostream>
#include <string>
#include <jsoncons/json.hpp>
#include <protocol.h>
#include <connection.h>
#include <game_logic.h>
#include <config/config_block.h>

using namespace hwo_protocol;

int main(int argc, const char* argv[])
{
    if (argc != 5) {
        fprintf(stderr, "Usage: ./run host port botname botkey\n");
        return 1;
    }

    const std::string host(argv[1]);
    const std::string port(argv[2]);
    const std::string name(argv[3]);
    const std::string key(argv[4]);
    printf("Host: %s, port: %s, name: %s, key: %s\n", host.c_str(), port.c_str(), name.c_str(), key.c_str());
    game_logic game(host, port, name, key);

    return 0;
}
