#pragma once

#define RAD2DEG(a) a/M_PI*180
#define DEG2RAD(a) a/180*M_PI

#define IS_JSON_OBJECT(j) \
{\
    j.is<jsoncons::json::object>()\
}
