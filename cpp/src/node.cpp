#include <node.h>

namespace hwo
{

Node::Node(const std::string& name, const std::shared_ptr<ChannelMap>& channels) :
    name_(name),
    channels_(channels)
{
}

void Node::spin()
{
}

} // namespace hwo
