#ifndef utils_h
#define utils_h

#include <cmath>

#define RAD2DEG(a) a/M_PI*180
#define DEG2RAD(a) a/180*M_PI

namespace hwo
{

inline double NormalizeAngle(double angle_rad)
{
    const double angle_max_rad = M_PI;
    const double angle_min_rad = -M_PI;
    if (fabs(angle_rad) > 2.0 * M_PI) { // normalize to [-2*pi, 2*pi] range
        angle_rad -= ((int)(angle_rad / (2.0 * M_PI))) * 2.0 * M_PI;
    }

    while (angle_rad > angle_max_rad) {
        angle_rad -= 2.0 * M_PI;
    }

    while (angle_rad < angle_min_rad) {
        angle_rad += 2.0 * M_PI;
    }

    return angle_rad;
}


template <typename T> int Sign(T val) {
    return (T(0) < val) - (val < T(0));
}

} // namespace hwo

#endif
