#ifndef LaneInfo_h
#define LaneInfo_h

#include <vector>

namespace hwo
{

class LaneInfo
{
public:

    LaneInfo(int index, double dist) : index_(index), dist_(dist) { }

    int index() const { return index_; }
    double dist() const { return dist_; }

private:

    int index_;
    double dist_;
};

class LanesInfo
{
public:

    LanesInfo(const std::vector<LaneInfo>& lanes);

    const std::vector<LaneInfo>& lanes() const { return lanes_; }

private:

    std::vector<LaneInfo> lanes_;
};

} // namespace hwo

#endif
