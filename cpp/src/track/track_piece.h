#pragma once
#include <constants.h>
#include <jsoncons/json.hpp>
#include <boost/shared_ptr.hpp>

namespace hwo{

class TrackPiece {
    public:
        
        virtual void length(double radius, double total_angle)=0;
        double inline length() { return length_; }

        bool inline switchable() { return switchable_; }
        bool inline switchable(bool switchable) { switchable_ = switchable; }

        // Is there a cleaner way of doing this?
        virtual double radius() { return 0; }
        virtual double angle() {return 0; }

        static std::shared_ptr<TrackPiece> makeTrackPiece(const jsoncons::json& data);

        virtual void printInfo() = 0;

    protected:
        double length_;
        bool switchable_;
};


class TurnPiece : public TrackPiece {
    public:
        TurnPiece(const jsoncons::json& data);
        void length(double radius, double angle);
        void printInfo();
        double inline radius(){ return radius_; }
        double inline angle(){ return angle_; }
    private:
        double radius_;
        double angle_;
};


class StraightPiece : public TrackPiece {
    public:
        void printInfo();
        StraightPiece(const jsoncons::json& data);
        void length(double length, double angle = 0) { length_ = length;}
};

typedef std::shared_ptr<TrackPiece> TrackPiecePtr;

}  // namespace hwo