#ifndef Piece_h
#define Piece_h

#include <vector>
#include <track/piece_info.h>
#include <track/lane_info.h>

namespace hwo
{

class Piece
{
public:

    Piece(const PieceInfo& info, const LanesInfo& lanes);

    /// @{ Lane-related information
    double length(int lane) const;
    bool left_switch(int lane) const;
    bool right_switch(int lane) const;
    /// @}

    double radius() const { return info_.radius(); }
    // Note: Angle is saved in radians.
    double angle() const { return info_.angle(); }

    bool left_turn() const { return info_.angle() < 0.0; }
    bool right_turn() const { return info_.angle() > 0.0; }

    bool is_straight() const { return info_.is_straight(); }
    bool is_bend() const { return info_.is_bend(); }

    const LanesInfo& lanes_info() const { return lanes_info_; }

private:

    enum SwitchDir
    {
        INVALID_SWITCH_DIR,
        SWITCH_DIR_LEFT,
        SWITCH_DIR_RIGHT,
        NUM_SWITCH_DIRS
    };

    PieceInfo info_;
    LanesInfo lanes_info_;
    std::vector<double> lengths_;

    bool lane_switch(int lane, SwitchDir dir) const;
};

} // namespace hwo

#endif
