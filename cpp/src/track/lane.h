#ifndef Lane_h
#define Lane_h

#include <track/lane_info.h>
#include <track/piece.h>
#include <msg/car_position_msgs.h>

namespace hwo
{

class Lane
{
public:

    // TODO : Remove this
    Lane(const LaneInfo& info);
    Lane(const LaneInfo& info, const std::vector<Piece>& pieces, int lane);

    double dist() const { return info_.dist(); }
    int index() const { return info_.index(); }

    // returns the total length if nothing is provided.
    double length() const { return cumulative_length_.back(); }
    double length(int piece_idx) const { return cumulative_length_[piece_idx]; }
    
    double track_position (const car_position_msgs::PiecePosition&
        piece_position) const;
private:

    LaneInfo info_;
    std::vector<double> cumulative_length_;
};

} // namespace hwo

#endif
