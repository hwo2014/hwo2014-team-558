#ifndef Track_h
#define Track_h

#include <string>
#include <vector>
#include <map>
#include <track/piece.h>
#include <track/lane.h>
#include <msg/car_position_msgs.h>

namespace hwo
{

class LanesInfo;
class PiecesInfo;

class Track
{
public:

    Track() { } // TODO: remove me

    Track(const std::string& id,
          const std::string& name,
          const LanesInfo& lanes,
          const PiecesInfo& pieces);

    const std::string& id() const { return id_; }
    const std::string& name() const { return name_; }

    const std::vector<Piece> pieces() const { return pieces_; }
    const std::vector<Lane> lanes() const { return lanes_; }

    // Gives the position on the track from the time we started driving.
    double track_position(const car_position_msgs::PiecePosition& piece_position);

    void printInfo();

private:

    std::string id_;
    std::string name_;
    std::vector<Piece> pieces_;
    std::vector<Lane> lanes_;
    // Mapping : lane_map_[hwo_index] = position in vector.
    std::map<int,int> lane_map_;
};

} // namespace hwo

#endif
