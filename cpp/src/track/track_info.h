#pragma once
#include <jsoncons/json.hpp>
#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <track/track_piece.h>
#include <msg/car_position_msgs.h>

// using namespace hwo::car_position_msgs;

namespace hwo{

class TrackInfo {
    public:
        TrackInfo(const jsoncons::json& data);
        
        void printInfo();

        //// @{ Per-piece Properties

        inline double piece_length(int piece_id) {
            return track_pieces_[piece_id]->length();
        }

        inline double piece_radius(int piece_id) {
            return track_pieces_[piece_id]->radius();
        }

        inline double piece_angle(int piece_id) {
            return track_pieces_[piece_id]->angle();
        }

        /// @}

        // track properties - cumulative properties
        double track_position(const car_position_msgs::PiecePosition& piece_position);

    private:
        std::string id_;
        std::string name_;
        std::vector<TrackPiecePtr> track_pieces_;

        // cumulative_length_[i] gives
        // the length of the track at the END of track_piece_i
        std::vector<double> cumulative_length_;  
        
        // TODO(Sid): Add LaneInfo
};

typedef std::shared_ptr<TrackInfo> TrackInfoPtr;

}  // namespace hwo
