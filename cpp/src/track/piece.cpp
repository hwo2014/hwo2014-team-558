#include <track/piece.h>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cmath>

namespace hwo
{

Piece::Piece(const PieceInfo& info, const LanesInfo& lanes) :
    info_(info),
    lanes_info_(lanes),
    lengths_()
{
    switch (info.type()) {
    case PieceInfo::TYPE_STRAIGHT:
    {
        // all lanes on the piece have the same length
        lengths_.resize(lanes.lanes().size());
        for (size_t i = 0; i < lanes.lanes().size(); ++i) {
            lengths_[i] = info.length();
        }
    }   break;
    case PieceInfo::TYPE_BEND:
    {
        // From Docs: A positive value tells that the lanes is to the right \
        // from the center line while a negative value indicates a lane to \
        // the left from the center.

        // The dist() is subtracted from the radius.
        lengths_.resize(lanes.lanes().size());
        for (size_t i = 0; i < lanes.lanes().size(); ++i) {
            const LaneInfo& lane_info = lanes.lanes()[i];
            lengths_[i] = (info.radius() - lane_info.dist()) * std::fabs(info.angle());
        }
    }   break;
    default:
        assert(false);
        break;
    }
}

double Piece::length(int lane) const
{
    assert(lane >= 0 && lane < lengths_.size());
    return lengths_[lane];
}

bool Piece::left_switch(int lane) const
{
    return lane_switch(lane, SWITCH_DIR_LEFT);
}

bool Piece::right_switch(int lane) const
{
    return lane_switch(lane, SWITCH_DIR_RIGHT);
}

bool Piece::lane_switch(int lane, SwitchDir dir) const
{
    assert(lane >= 0 && lane < (int)lengths_.size() &&
           dir > INVALID_SWITCH_DIR && dir < NUM_SWITCH_DIRS);

    if (!info_.has_switch()) {
        return false;
    }

    if (dir == SWITCH_DIR_LEFT) {
        return lane > 0;
    }
    else {
        return lane < lengths_.size() - 1;
    }
}

} // namespace hwo
