#include <track/track.h>

namespace hwo {

Track::Track(const std::string& id,
          const std::string& name,
          const LanesInfo& lanes,
          const PiecesInfo& pieces) :
    id_(id),
    name_(name)
{
    // Make the pieces
    for (auto& piece : pieces.pieces())
    {
        pieces_.push_back(Piece(piece, lanes));
    }

    // Make the lanes
    // Note: The lanes are sorted in increasing order of the dist()
    // Pass in the current value so that the lane is aware of where it is.
    // This information is used in getting the length of the piece along that
    // lane and for nothing else. It isn't saved.
    for (size_t i = 0; i < lanes.lanes().size(); ++i)
    {
        lanes_.push_back(Lane(lanes.lanes()[i], pieces_, i));
        lane_map_[lanes.lanes()[i].index()] = i;
    }
}

double Track::track_position(const car_position_msgs::PiecePosition& piece_position) {
    // Need to get the length along the current lane.
    int piece_index = piece_position.piece_index;
    int start_lane = lane_map_[piece_position.lane.start_lane_index];
    int end_lane = lane_map_[piece_position.lane.end_lane_index];
    double track_position = lanes_[start_lane].track_position(piece_position);
    
    // If you're on a switch, just return some fixed value for now.
    if(start_lane != end_lane) {
        double fudge_param = 0.5;  // TODO: FIXME; The fudge param that simply
        // makes the track_length remain the same during switches.
        double track_position_end = lanes_[end_lane].track_position(piece_position);

        track_position = (1 - fudge_param)*
        lanes_[start_lane].length(piece_index -1) + (fudge_param) *
        lanes_[end_lane].length(piece_index - 1);
    }
    
    return track_position;
}

void Track::printInfo() {
    printf("[Track::printInfo] Number of lanes: %d\n", lanes_.size());
    printf("[Track::printInfo] Number of pieces: %d\n", pieces_.size());
}

}  // namespace hwo