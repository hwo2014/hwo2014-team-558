#include <track/lane.h>
#include <cstdio>

using namespace std;

namespace hwo
{

Lane::Lane(const LaneInfo& info) : info_(info)
{ 
}

Lane::Lane(const LaneInfo& info, const std::vector<Piece>& pieces, int lane) :
    info_(info)
{
    // Compute the cumulative lengths
    cumulative_length_.resize(pieces.size());
    cumulative_length_[0] = pieces[0].length(lane);
    for (size_t i = 1; i < cumulative_length_.size(); ++i)
    {
        cumulative_length_[i] += cumulative_length_[i-1] + 
            pieces[i].length(lane);
    }
}

double Lane::track_position(const car_position_msgs::PiecePosition&
        piece_position) const
{
    double track_position = piece_position.lap * length() +
    piece_position.in_piece_distance;
    track_position += (piece_position.piece_index ==
        0)?(0):(length(piece_position.piece_index - 1));
    return track_position;
}

} // namespace hwo
