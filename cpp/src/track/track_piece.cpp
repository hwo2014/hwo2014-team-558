#include <memory>
#include <track/track_piece.h>
using namespace jsoncons;

namespace hwo{

TrackPiecePtr TrackPiece::makeTrackPiece(const jsoncons::json& data) {
    if (data.has_member("length")) {
        return std::make_shared<StraightPiece>(data);
    } else {
        return std::make_shared<TurnPiece>(data);
    }
}


TurnPiece::TurnPiece(const json& data) {
    radius_ = data["radius"].as<double>();
    angle_ = data["angle"].as<double>();
    length(radius_, angle_);
    switchable(data.get("switch", false).as<bool>());
}

void TurnPiece::printInfo() {
    printf("Turn piece :\t radius : %f \t angle : %f \t length : %f \t switch :"
        "%d\n", radius_, angle_, length_, switchable_);
}

void TurnPiece::length(double radius, double angle) {
    length_ = std::abs(radius * DEG2RAD(angle));
}

StraightPiece::StraightPiece(const json& data) {
    length(data["length"].as<double>());
    switchable(data.get("switch", false).as<bool>());
}

void StraightPiece::printInfo() {
    printf("Straight piece :\t length : %f \t switch : %d\n", length_,
        switchable_);
}


}  //namespace hwo