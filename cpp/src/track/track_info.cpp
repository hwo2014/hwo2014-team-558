#include <track/track_info.h>
#include <jsoncons/json.hpp>
using namespace jsoncons;

namespace hwo{

TrackInfo::TrackInfo(const jsoncons::json& data)
{
    printf("Initializing Track info\n");

    // Track name and id
    id_ = data.get("id","").as<std::string>();
    name_ = data.get("name","").as<std::string>();
    printf("Track name: %s, Track id : %s\n", name_.c_str(), id_.c_str());

    // Make pieces
    for (size_t i = 0; i < data["pieces"].size(); ++i) {
        track_pieces_.push_back(TrackPiece::makeTrackPiece(data["pieces"][i]));
    }

    // Init cumulative sums for track length
    cumulative_length_.resize(track_pieces_.size());
    cumulative_length_[0] = track_pieces_[0]->length();
    for (size_t i = 1; i < track_pieces_.size(); ++i)
    {
        cumulative_length_[i] = cumulative_length_[i-1] +
        track_pieces_[i]->length();
        printf("Cumulative: %d : %f\n", i, cumulative_length_[i]);
    }


    printf("Initialized %d track pieces.\n", static_cast<int>(track_pieces_.size()));
}

// Note: This returns the cumulative length from the beginning of the race
// (includes lap information). This is so that when you finish a lap, your
// velocity doesn't suddenly jump.
double TrackInfo::track_position(const car_position_msgs::PiecePosition& piece_position) {
    int id = piece_position.piece_index;
    double track_position = ((piece_position.lap*cumulative_length_.back()) +
        piece_position.in_piece_distance);

    track_position += (piece_position.piece_index == 0) ? 
        (0) : (cumulative_length_[piece_position.piece_index - 1]);
    return track_position;
}


void TrackInfo::printInfo() {
    for (size_t i = 0; i < track_pieces_.size(); ++i) {
        track_pieces_[i]->printInfo();
    }
}

}  // namespace hwo
