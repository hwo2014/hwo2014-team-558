#include <algorithm>
#include <track/lane_info.h>

namespace hwo
{

LanesInfo::LanesInfo(const std::vector<LaneInfo>& lanes) :
    lanes_(lanes)
{
    std::sort(lanes_.begin(), lanes_.end(),
              [](const LaneInfo& lane1, const LaneInfo& lane2)
              {
                  return lane1.dist() < lane2.dist();
              });
}

} // namespace hwo
