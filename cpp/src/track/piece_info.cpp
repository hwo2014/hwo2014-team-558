#include <cmath>
#include <limits>
#include <track/piece_info.h>
#include <utils/utils.h>

namespace hwo
{

PieceInfo::PieceInfo(double length, bool has_switch) :
    radius_(std::numeric_limits<double>::infinity()),
    angle_(0.0),
    length_(length),
    has_switch_(has_switch),
    type_(TYPE_STRAIGHT)
{
}

PieceInfo::PieceInfo(double radius, double angle, bool has_switch) :
    radius_(radius),
    angle_(DEG2RAD(angle)),
    length_(radius_ * fabs(NormalizeAngle(angle_))),
    has_switch_(has_switch),
    type_(TYPE_BEND)
{
}

} // namespace hwo
