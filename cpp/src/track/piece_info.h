#ifndef PieceInfo_h
#define PieceInfo_h

#include <vector>

namespace hwo
{

class PieceInfo
{
public:

    enum Type
    {
        INVALID_TYPE = -1,
        TYPE_STRAIGHT,
        TYPE_BEND,
        NUM_TYPES
    };

    /// @brief Construct PieceInfo representing a straight piece.
    PieceInfo(double length, bool has_switch);

    /// @brief Construct PieceInfo representing a bend piece.
    PieceInfo(double radius, double angle, bool has_switch);

    double radius() const { return radius_; }
    double angle() const { return angle_; }
    bool left_turn() const { return angle_ < 0.0; }
    bool right_turn() const { return angle_ > 0.0; }

    /// @brief Return the length of the center lane.
    double length() const { return length_; }

    /// @brief Return whether this track has a switch
    bool has_switch() const { return has_switch_; }

    bool is_straight() const { return type_ == TYPE_STRAIGHT; }
    bool is_bend() const { return type_ == TYPE_BEND; }
    Type type() const { return type_; }

private:

    double radius_;
    double angle_;

    double length_;
    bool has_switch_;
    Type type_;
};

class PiecesInfo
{
public: 

    PiecesInfo(const std::vector<PieceInfo>& pieces) :
        pieces_(pieces) {}

    const std::vector<PieceInfo>& pieces() const { return pieces_; }

private:

    std::vector<PieceInfo> pieces_;
};

} // namespace hwo

#endif
