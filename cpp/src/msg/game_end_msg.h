#ifndef hwo_GameEndMessage_h
#define hwo_GameEndMessage_h

#include <string>
#include <vector>

namespace hwo
{

struct GameEndResult
{
    struct Car { std::string name; std::string color; } car;
    struct Result { int laps; int ticks; int millis; } result;
};

struct GameEndMessage
{
    std::vector<GameEndResult> results;
};

} // namespace hwo

#endif
