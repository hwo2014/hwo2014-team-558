#ifndef hwo_json_msg_h
#define hwo_json_msg_h

#include <string>

namespace hwo
{

struct JsonMessage
{
    std::string msg;
};

} // namespace hwo

#endif
