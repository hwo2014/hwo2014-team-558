#ifndef hwo_DummyMessage_h
#define hwo_DummyMessage_h

namespace hwo
{

struct DummyMessage
{
    int an_int;
    double a_double;
    bool a_bool;
};

} // namespace hwo

#endif
