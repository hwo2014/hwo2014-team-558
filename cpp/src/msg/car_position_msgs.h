#ifndef hwo_CarPositionMessage_h
#define hwo_CarPositionMessage_h
#include <string>
#include <vector>
#include <chrono>

namespace hwo
{
namespace car_position_msgs {

struct ID
{
    std::string name;
    std::string color;
};

struct LaneData
{
    int start_lane_index;
    int end_lane_index;
};

struct PiecePosition
{
    int piece_index;
    double in_piece_distance;
    LaneData lane;
    int lap;
};

struct CarPosition
{
    ID id;
    double angle;
    PiecePosition piece_position;
};

struct CarPositionMessage
{
    std::chrono::time_point<std::chrono::high_resolution_clock> stamp;
    int game_tick; 
    std::vector<CarPosition> car_positions;
};

} // namespace car_position_msgs
} // namespace hwo

#endif
