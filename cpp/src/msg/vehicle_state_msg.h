#ifndef hwo_VehicleStatesMessage_h
#define hwo_VehicleStatesMessage_h

#include <vector>
#include <msg/car_position_msgs.h>

namespace hwo
{

struct VehicleStateMessage
{
    car_position_msgs::CarPosition car_position;
    double speed_mps;
};

struct VehicleStatesMessage
{
    std::vector<VehicleStateMessage> vehicle_states;
};

} // namespace hwo

#endif
