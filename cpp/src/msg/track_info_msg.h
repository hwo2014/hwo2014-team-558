#ifndef hwo_TrackInfoMessage_h
#define hwo_TrackInfoMessage_h
#include <string>
#include <vector>
#include <chrono>
#include <track/track.h>

namespace hwo
{
namespace track_info_msgs {

struct TrackInfoMessage
{
    std::chrono::time_point<std::chrono::high_resolution_clock> stamp;
    std::shared_ptr<Track> track;
};

} // namespace track_info_msgs

} // namespace hwo

#endif
